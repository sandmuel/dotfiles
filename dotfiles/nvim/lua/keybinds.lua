vim.cmd("nnoremap <C-s> :w<cr>") -- save
vim.cmd("inoremap <C-s> <esc>:w<cr>a") -- exit insert mode then save

vim.cmd("behave mswin")
vim.cmd('smap <Del> <C-g>"_d')
vim.cmd('smap <C-c> <C-g>y')
vim.cmd('smap <C-x> <C-g>x')
vim.cmd('imap <C-v> <Esc>pi')
vim.cmd('smap <C-v> <C-g>p')
vim.cmd('smap <Tab> <C-g>1>')
vim.cmd('smap <S-Tab> <C-g>1<')
