require("neo-tree").setup({
	source_selector = {
		winbar = true
	}
})
vim.cmd("ToggleTerm")
vim.cmd("Neotree")
require("bufferline").setup({
	options = {
		offsets = {
			{
				filetype = "neo-tree",
				text = "Files",
				highlight = "Directory",
				text_align = "center",
				separator = true
			}
		}
	}
})
