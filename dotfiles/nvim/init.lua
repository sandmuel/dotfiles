vim.cmd("set number")

require("lazyvim")

require("better-tab-width")
require("keybinds")
require("theme")
require("layout")

require("cmp").setup({})

-- Map left and right arrow keys to move to previous and next lines
vim.api.nvim_set_keymap('n', '<Left>', ':call MoveLeftOrPreviousLine()<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Right>', ':call MoveRightOrNextLine()<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('i', '<Left>', '<C-O>:call MoveLeftOrPreviousLine()<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('i', '<Right>', '<C-O>:call MoveRightOrNextLine()<CR>', { noremap = true, silent = true })

-- Define the MoveLeftOrPreviousLine() function
vim.cmd([[
  function! MoveLeftOrPreviousLine()
    if col('.') == 1
      normal k$
    else
      normal h
    endif
  endfunction
]])

-- Define the MoveRightOrNextLine() function
vim.cmd([[
  function! MoveRightOrNextLine()
    if col('.') == col('$') - 1
      normal j0
    else
      normal l
    endif
  endfunction
]])
